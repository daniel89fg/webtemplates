<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebTemplates;

use FacturaScripts\Core\Base\InitClass;

/**
 * Description of Init
 *
 * @author Athos Online <info@athosonline.com>
 */
class Init extends InitClass
{

    public function init()
    {
        
    }

    public function update()
    {
        
    }
}